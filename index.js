const
    fastifyPlugin = require('fastify-plugin'),
    { default: PQueue } = require('p-queue'),
    axios = require('axios'),
    { nanoid } = require('nanoid'),
    { crc64 } = require('crc64-ecma'),
    dayjs = require('dayjs');

const queue = new PQueue({ concurrency: 1 });

module.exports = fastifyPlugin((
    /** @type {import('fastify').FastifyInstance} */
    fastify,
    {
        /** @type {string} */
        url,
        /** @type {number} */
        siteId,
        /** @type {string} */
        token,
        /** @type {Function} */
        onError = () => {},
        /** @type {('onResponse'|'preHandler')} */
        hook = 'onResponse'
    },
    next
) => {
    const client = axios.create({ baseURL: url });
    client.interceptors.request.use(config => ({
        ...config,
        params: {
            ...config.params,
            'idsite': siteId,
            'rec': 1,
            'rand': nanoid(),
            'apiv': 1,
            'token_auth': token
        }
    }));
    fastify.addHook(
        hook,
        (
            request,
            reply,
            done
        ) => {
            const
                currentDate = dayjs(),
                responseTime = Math.round(reply.getResponseTime());
            queue
                .add(async () => {
                    await client(
                        'matomo.php',
                        {
                            method: 'POST',
                            params: {
                                'action_name': `${request.routerMethod} ${request.routerPath}`,
                                'url': `${request.protocol}://${request.hostname}${request.url}`,
                                '_id': crc64(request.ip + request.headers['user-agent']).toString(16),
                                'urlref': request.headers['referer'],
                                'ua': request.headers['user-agent'],
                                'lang': request.headers['accept-language'],
                                'cip': request.ip,
                                'cdt': {
                                    'onResponse': dayjs(currentDate.valueOf() - responseTime).unix(),
                                    'preHandler': currentDate.unix()
                                }[hook],
                                ...hook === 'onResponse' ? {
                                    'pf_srv': responseTime
                                } : {}
                            }
                        }
                    );
                })
                .catch(onError);
            done();
        }
    );
    next();
});